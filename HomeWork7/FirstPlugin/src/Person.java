public class Person implements Plugin {
    private String name;
    private String surname;

    public Person(){
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Person(String name, String surname){
        this.name = name;
        this.surname = surname;
    }

    public String toString(){
        return name + " " + surname;
    }

    @Override
    public void doUsefull() {
        setName("Ivan");
        setSurname("Ivanov");
        System.out.println(this);
    }
}
